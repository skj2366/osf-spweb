/**
 * 
 */
var AjaxUtil = function(conf) {
	conf.method = conf.method ? conf.method : 'GET';
	var xhr = new XMLHttpRequest();
	
	if (!conf.url) {
		alert('url은 필수입니다.');
		return;
	}
	
	xhr.open(conf.method,conf.url);
	xhr.onreadystatechange = function(){
		if(xhr.readyState==xhr.DONE){
			if(xhr.status==200){
				conf.suc(xhr.response);
			}else{
				conf.err();
			}
		}
	}
	
	this.setConf = function(conf){
		conf = conf;
	}
	
	this.send = function(data){
		if(conf.method!='GET'){
			xhr.setRequestHeader('content-type','application/json');
		}
		xhr.send(data);
	}
}