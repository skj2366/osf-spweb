<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
<script src="${rPath}/resources/js/AjaxUtil.js"></script>
</head>

<body>

	<div id="deptGrid" style="width: 500px; height: 200px;"></div>
	<br>
	<div id="empGrid" style="width: 500px; height: 200px;"></div>
	<button id="dogfoot" onclick="updateEmp()">수정</button> 
	<script>
		
		var deptGrid = new dhtmlXGridObject('deptGrid');
		deptGrid.setHeader('부서번호,부서이름,지역,',null,[ "text-align:center;","text-align:center;", "text-align:center;" ]);
		deptGrid.setColumnIds('deptNo,dname,loc');
		deptGrid.setColTypes('ed,ed,ed');
		deptGrid.setColAlign('center,center,center');
		deptGrid.init();

		var empGrid = new dhtmlXGridObject('empGrid');
		empGrid.setHeader('번호,이름,부서번호,직급',null,[ "text-align:center;","text-align:center;", "text-align:center;" ]);
		empGrid.setColumnIds('empNo,ename,deptNo,job');
		empGrid.setColTypes('ro,ed,ro,ed');
		deptGrid.setColAlign('center,center,center,center');
		empGrid.init();
		deptGrid.attachEvent('onRowSelect',function(rId,ind){
			var data = deptGrid.getRowData(rId);
			var au = new AjaxUtil({url:'/empdept2?deptNo='+data.deptNo,suc:callback2});
			au.send();
		})

		function callback(res){
			deptGrid.parse(res,'js');
			deptGrid.selectRow(0);
			var rowId = deptGrid.getSelectedRowId();
			var data = deptGrid.getRowData(rowId);
			console.log(data.deptNo);
			var au = new AjaxUtil({url:'/empdept2?deptNo='+data.deptNo,suc:callback2});
			au.send();
		}

		function callback2(res){
			empGrid.clearAll();	
			empGrid.parse(res,'js');
		}
		
		var au = new AjaxUtil({
			url : '/depts',
			suc : callback
		});
		au.send();
		
		var rIds = [];
		
		function updateEmp(){
			var emps = [];
			for(var rId of rIds){
				emps.push(grid.getRowData(rId));
				
			}
			console.log(emps);
			var xhr = new XMLHttpRequest();
			xhr.open('PUT','/empdept2');
			xhr.setRequestHeader('content-type','application/json');
			xhr.onreadystatechange = function(){
				if(xhr.readuState==xhr.DONE){
					if(xhr.status==200){
						alert('수정성공');
					}else{
						alert('수정실패');
					}
				}
			}
			xhr.send();
			
		} 
		
		
		empGrid.attachEvent('onEditCell',function(stage, rId, ind, nV, oV){
			var rStatus = empGrid.getRowAttribute(rId,'status');
			if(rStatus=='n'){
				return true;
			}
			//alert(stage + ' ' + rId + ' ' + ind + ' ' + nV + ' ' + oV);
			if(stage==2){
				var result=false;
				if(nV!=oV){
					var data=empGrid.getRowData(rId);
					console.log(data);
					data = JSON.stringify(data);
					var xhr = new XMLHttpRequest();
					xhr.open('PUT', '${rPath}/empdept2',false);
					xhr.setRequestHeader('Content-type','application/json');
					xhr.onreadystatechange = function() {
						if (xhr.readyState == xhr.DONE) {
							if (xhr.status == 200) {
								//alert(xhr.response);
								if(xhr.response==='1'){
									alert('수정완료');
									result = true;	
								}else{
									alert('수정실패');
								}
							}
						}
					}
					xhr.send(data);
					return result;
				}	
			}
		});
	</script>
</body>
</html> --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
<script src="${rPath}/resources/js/AjaxUtil.js"></script>
</head>
<body>
	<div id="deptGrid" style="width: 400px; height: 200px;"></div>
	<div id="empGrid" style="width: 400px; height: 450px;"></div>
	<button onclick="savede()">저장</button>

	<script>
		function savede(){
			var ids = empGrid.getChangedRows();
			if(!ids){
				alert('수정 사항이 없습니다.');
				return;
			}
			var rows = []
			ids = ids.split(",");
			for(var id of ids){
				rows.push(empGrid.getRowData(id));
			}
			console.log(rows);
			var au = new AjaxUtil({
				method:'PUT',
				url:'/empdept2',
				suc:function(res){
					if(res==ids.length){
						alert('저장성공');
					}
				}
			});
			au.send(JSON.stringify(rows));

		}
		var deptGrid = new dhtmlXGridObject('deptGrid');
		deptGrid.setHeader('부서번호,부서이름,근무지역', null, [ "text-align:center;",
				"text-align:center;", "text-align:center; " ]);
		deptGrid.setColumnIds('deptno,dname,loc');
		deptGrid.setColTypes('ro,ed,ed');
		deptGrid.init();

		var empGrid = new dhtmlXGridObject('empGrid');
		empGrid.setHeader('사원번호,사원이름,부서번호,직급', null, [ "text-align:center;",
				"text-align:center;", "text-align:center; " ]);
		empGrid.setColumnIds('empno,ename,deptno,job');
		empGrid.setColTypes('ro,ed,ro,ed');
		empGrid.init();

	
		deptGrid.attachEvent('onRowSelect', function(rId, ind) {
			var data = deptGrid.getRowData(rId);
			var au = new AjaxUtil({
				url : '/empdept2?deptno=' + data.deptno,
				suc : callback2
			});
			au.send();
		});

		function callback(res) {
			deptGrid.parse(res, 'js');
			deptGrid.selectRow(0);
			var rowId = deptGrid.getSelectedRowId();
			var data = deptGrid.getRowData(rowId);
			var au = new AjaxUtil({
				url : '/empdept2?deptno=' + data.deptno,
				suc : callback2
			});
			au.send();
		}
		function callback2(res) {

			empGrid.clearAll();
			empGrid.parse(res, 'js');
		}
		
		var au = new AjaxUtil({
			url : '/depts',
			suc : callback
		});
		au.send();
	</script>
</body>
</html>