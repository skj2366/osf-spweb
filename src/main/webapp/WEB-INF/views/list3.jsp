<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
</head>
<body>
	${empList3}
	<div id="gridbox" style="width: 700px; height: 400px;"></div>
	사원번호 : <input type="number" id="empNo"> 
	사원이름 : <input type="text" id="eName"> 
	직급 : <input type="text" id="job">
	<button onclick="searchEmp()">검색</button>
	<button onclick="saveEmp()">저장</button>
	<button onclick="saveEmps()">한번에저장</button>
	<script>
		
		function saveEmps(){
			var emps=[];
			for(var rId of rIds){
				emps.push(grid.getRowData(rId));
			}
			var xhr = new XMLHttpRequest();
			xhr.open('POST','${rPath}/emps',false);
			xhr.setRequestHeader('Content-type','application/json');
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						if(xhr.response==rIds.length){
							alert('삽입완료');	
						}else{
							alert('삽입실패');
						}	
					}
				}
			}
			xhr.send(JSON.stringify(emps)); 
		}
		
		function saveEmp(){
			for(var rId of rIds){
				var data = JSON.stringify(grid.getRowData(rId));
				var xhr = new XMLHttpRequest();
				xhr.open('POST','${rPath}/emp',false);
				xhr.setRequestHeader('Content-type','application/json');
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if(xhr.response==='1'){
								alert('삽입완료');	
							}else{
								alert('삽입실패');
							}	
						}
					}
				}
				xhr.send(data); 
			}
			return;
			
		}
		
		var rIds = [];
		var menuForm = [{id:'add',text:'추가'},{id:'del',text:'삭제'}];
		var dxMenu = new dhtmlXMenuObject();
		dxMenu.setIconsPath('${dPath}/codebase/imgs/');
		dxMenu.renderAsContextMenu();
		dxMenu.attachEvent('onClick',function(id,type){
			if(id=='add'){

				var rId = grid.uid();//
				grid.addRow(rId,{},grid.getRowsNum());
				grid.setRowAttribute(rId,'status','n');
				rIds.push(rId);
				/* alert(rId); */
			}else if(id=='del'){
				var rId=grid.getSelectedRowId();
				if(rId==null){
					alert('삭제할 로우를 선택해주세요.');
					return;
				}
				var ind = grid.getColIndexById('empNo');
				var empNo = grid.cells(rId,ind).getValue();
				//alert(empNo);
				var xhr = new XMLHttpRequest();
				xhr.open('delete', '${rPath}/empdept2/' + empNo,false);
				var res = false;
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if(xhr.response==='1'){
								alert(empNo + '삭제완료');
								location.href='${rPath}/emp/list';
								res = true;	
							}
						}
					}
				}
				xhr.send();
				return res;
			}
		});
		dxMenu.loadStruct(menuForm);
		
		var grid = new dhtmlXGridObject('gridbox');
		grid.setImagePath('${dPath}/codebase/imgs/');
		grid.setHeader('empNo,eName,job', null, [ "text-align:center;","text-align:center;", "text-align:center;" ]);
		grid.setColumnIds('empNo,ename,job');//키값이라서 스페이스를 빼야한다!!!
		grid.setColAlign('center,center,center');
		grid.setColTypes('ro,ed,ed');
		grid.setColSorting('int,str,str');
		grid.enableContextMenu(dxMenu);
		grid.init();

		/* var data = JSON.parse('${empList3}');
		grid.parse(data,'js'); */

		grid.attachEvent('onRowSelect', function(id, ind) {
			//alert('id : ' + id); // 로우 번호
			//alert('ind : ' + ind);//셀 번호 

			var empNo = this.cells(id, 0).getValue();
			//alert('selected empNo : ' + empNo); //선택한것의 empNo 출력
			document.querySelector('#empNo').value = empNo; //선택한것의 empNo를 input박스에 출력시킨다.
			var eName = this.cells(id, 1).getValue();
			document.querySelector('#eName').value = eName;
			var job = this.cells(id, 2).getValue();
			document.querySelector('#job').value = job;
			/* var deptNo = this.cells(id,3).getValue();
			document.querySelector('#deptNo').value = deptNo; */
		});

		grid.attachEvent('onEditCell',function(stage, rId, ind, nV, oV){
			var rStatus = grid.getRowAttribute(rId,'status');
			if(rStatus=='n'){
				return true;
			}
			if(stage==2){
				var result=false;
				if(nV!=oV){
					var data=grid.getRowData(rId);
					console.log(data);
					data = JSON.stringify(data);
					var xhr = new XMLHttpRequest();
					xhr.open('PUT', '${rPath}/empdept2',false);
					xhr.setRequestHeader('Content-type','application/json');
					xhr.onreadystatechange = function() {
						if (xhr.readyState == xhr.DONE) {
							if (xhr.status == 200) {
								if(xhr.response==='1'){
									alert('수정완료');
									result = true;	
								}
							}
						}
					}
					xhr.send(data);
					return result;
				}	
			}
		});
		
		function makeParam(){
			var inputs = document.querySelectorAll('input')
			var param = '';
			for(var input of inputs){
				if(input.value.trim()!=''){
					param += input.id + '=' + input.value + '&';	
				}
			}
			return param;
		}
		
		function searchEmp() {
			/* alert(makeParam()); */
			var url = '${rPath}/empdept2?' + makeParam();
			var xhr = new XMLHttpRequest();
			/* xhr.open('GET', '${rPath}/empdept2'); */
			xhr.open('GET', url);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						grid.clearAll();
						grid.parse(xhr.response, 'js');
					}
				}
			}
			xhr.send();
		}
		searchEmp();
		
		var test = function(func){
			func(1,2,3,4);
		}
		
		test(function(a,b,c,d){
			console.log(a+','+b+','+c+','+d);
		})
	</script>
</body>
</html>