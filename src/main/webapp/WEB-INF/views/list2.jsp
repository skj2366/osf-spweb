<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<table border="1">
	<tr>
		<th>번호</th>
		<th>이름</th>
		<th>직급</th>
		<th>상사</th>
		<th>입사</th>
		<th>급여</th>
		<th>부서</th>
		<th>근지</th>
		
		<c:forEach items="${empList2}" var="e">
			<tr>
				<td>${e.EMPNO}</td>
				<td>${e.ENAME}</td>
				<td>${e.JOB}</td>
				<td>${e.MGR}</td>
				<td>${e.HIREDATE}</td>
				<td>${e.SAL}</td>
				<td>${e.DNAME}</td>
				<td>${e.LOC}</td>
			</tr>
		</c:forEach>
	</tr>
</table>
</body>
</html>