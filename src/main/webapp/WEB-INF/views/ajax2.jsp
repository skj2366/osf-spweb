<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
<script src="${rPath}/resources/js/AjaxUtil.js"></script>
</head>

<body>

	<div id="deptGrid" style="width: 500px; height: 200px;"></div>
	<br>
	<div id="empGrid" style="width: 500px; height: 200px;"></div>

	<script>
		var deptGrid = new dhtmlXGridObject('deptGrid');
		deptGrid.setHeader('번호,이름,지역');
		deptGrid.setColumnIds('deptNo,dname,loc');
		deptGrid.setColTypes('ro,ed,ed');
		deptGrid.init;

		var empGrid = new dhtmlXGridObject('empGrid');
		empGrid.setHeader('번호,이름,지역');
		empGrid.setColumnIds('empNo,ename,deptNo,job');
		empGrid.setColTypes('ro,ed,ed,ed');
		empGrid.init;

		function callback(res){
			deptGrid.parse(res,'js');
			deptGrid.selectRow(0);
			var rowId = deptGrid.getSelectedRowId();
			var data = deptGrid.getRowData(rowId);
			console.log(data);
			}

		var au = new AjaxUtil({
			url : '/depts',
			suc : callback
		});
		au.send();
	</script>
</body>
</html>