<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
</head>
<body>
	<div id="gridbox" style="width: 700px; height: 400px;"></div>
	deptNo : <input type="number" id="deptNo"> 
	dName :	<input type="text" id="dName"> 
	loc :	<input type="text" id="loc">
	<button onclick="searchDept()">검색</button>
	<button onclick="saveDept()">저장</button>
	<button onclick="saveDepts()">한번에저장</button>
	<script>
		
		function saveDepts(){
			var depts=[];
			for(var rId of rIds){
				depts.push(grid.getRowData(rId));
			}
			var xhr = new XMLHttpRequest();
			xhr.open('POST','${rPath}/depts',false);
			xhr.setRequestHeader('Content-type','application/json');
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						if(xhr.response==rIds.length){
							alert('삽입완료');	
						}else{
							alert('삽입실패');
						}	
					}
				}
			}
			xhr.send(JSON.stringify(depts)); 
		}
		
		function saveDept(){
			
			for(var rId of rIds){
				var data = JSON.stringify(grid.getRowData(rId));
				var xhr = new XMLHttpRequest();
				xhr.open('POST','${rPath}/dept',false);
				xhr.setRequestHeader('Content-type','application/json');
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if(xhr.response==='1'){
								alert('삽입완료');	
							}else{
								alert('삽입실패');
							}	
						}
					}
				}
				xhr.send(data); 
			}
			return;
			
		}
		
		var rIds = [];
		var menuForm = [{id:'add',text:'추가'},{id:'del',text:'삭제'}];
		var dxMenu = new dhtmlXMenuObject();
		dxMenu.setIconsPath('${dPath}/codebase/imgs/');
		dxMenu.renderAsContextMenu();
		dxMenu.attachEvent('onClick',function(id,type){
			if(id=='add'){

				var rId = grid.uid();
				grid.addRow(rId,{},grid.getRowsNum());
				grid.setRowAttribute(rId,'status','n');
				rIds.push(rId);
				
			}else if(id=='del'){
				var rId=grid.getSelectedRowId();
				if(rId==null){
					alert('삭제할 로우를 선택해주세요.');
					return;
				}
				var ind = grid.getColIndexById('deptNo');
				var deptNo = grid.cells(rId,ind).getValue();
				var xhr = new XMLHttpRequest();
				xhr.open('delete', '${rPath}/depts/' + deptNo,false);
				var res = false;
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if(xhr.response==='1'){
								alert(deptNo + ' 삭제완료');
								location.href='${rPath}/dept/list';
								res = true;	
							}
						}
					}
				}
				xhr.send();
				return res;
			}
		});
		dxMenu.loadStruct(menuForm);
		
		var grid = new dhtmlXGridObject('gridbox');
		grid.setImagePath('${dPath}/codebase/imgs/');
		grid.setHeader('deptNo,dName,loc', null, [ "text-align:center;","text-align:center;", "text-align:center;" ]);
		grid.setColumnIds('deptNo,dname,loc');
		grid.setColAlign('center,center,center');
		grid.setColTypes('ed,ed,ed');
		grid.setColSorting('int,str,str');
		grid.enableContextMenu(dxMenu);
		grid.init();


		grid.attachEvent('onRowSelect', function(id, ind) {

			var deptNo = this.cells(id, 0).getValue();
			document.querySelector('#deptNo').value = deptNo;
			var dName = this.cells(id, 1).getValue();
			document.querySelector('#dName').value = dName;
			var loc = this.cells(id, 2).getValue();
			document.querySelector('#loc').value = loc;
		});

		grid.attachEvent('onEditCell',function(stage, rId, ind, nV, oV){
			var rStatus = grid.getRowAttribute(rId,'status');
			if(rStatus=='n'){
				return true;
			}
			//alert(stage + ' ' + rId + ' ' + ind + ' ' + nV + ' ' + oV);
			if(stage==2){
				var result=false;
				if(nV!=oV){
					var data=grid.getRowData(rId);
					console.log(data);
					data = JSON.stringify(data);
					var xhr = new XMLHttpRequest();
					xhr.open('PUT', '${rPath}/depts',false);
					xhr.setRequestHeader('Content-type','application/json');
					xhr.onreadystatechange = function() {
						if (xhr.readyState == xhr.DONE) {
							if (xhr.status == 200) {
								alert(xhr.response);
								if(xhr.response==='1'){
									alert('수정완료');
									result = true;	
								}else{
									alert('수정실패');
								}
							}
						}
					}
					xhr.send(data);
					return result;
				}	
			}
		});
		
		function makeParam(){
			var inputs = document.querySelectorAll('input')
			var param = '';
			for(var input of inputs){
				if(input.value.trim()!=''){
					param += input.id + '=' + input.value + '&';	
				}
			}
			return param;
		}
		
		function searchDept() {
			var url = '${rPath}/depts?' + makeParam();
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						grid.clearAll();
						grid.parse(xhr.response, 'js');
					}
				}
			}
			xhr.send();
		}
		searchDept();
	</script>
</body>
</html>