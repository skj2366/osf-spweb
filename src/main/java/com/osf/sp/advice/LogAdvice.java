package com.osf.sp.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogAdvice {

	@Before("execution(* com.osf.sp.controller.*.*(..))")//콘드롤러.*(클래스싹다, 패키지는안돼).*{메서드싹다}(..{파라메터싹다}) --> 한마디로 싹다
	public void beforeLog(JoinPoint jp) {
		log.debug("오우 ~~ 난 무조건 실행");
	}
	
	@After("execution(* com.osf.sp.controller.*.*(..))")
	public void afterLog(JoinPoint jp) {
		log.debug("끝나고 무조건 실행");
		
	}
}
