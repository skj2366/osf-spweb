package com.osf.sp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.osf.sp.service.EmpService;
import com.osf.sp.vo.EmpVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class EmpController {
	
	@Autowired
	private EmpService es;
	
//	@RequestMapping(value="/empdept2", method=RequestMethod.GET)
//	public String getEmpDeptList2(Model m) {
//		ObjectMapper om = new ObjectMapper();
//		String json = "";
//		try {
//			json = om.writeValueAsString(es.getEmpDeptList2());
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		m.addAttribute("empList3",json);
//		log.info("empList3=>{}", json);
//		return "list3";
//	}
	
	@RequestMapping(value="/emp/list", method=RequestMethod.GET)
	public String goPage() {
		log.debug("goPage Start");
		log.info("goPage End");
		return "list3";
	}
	@RequestMapping(value="/empdept2", method=RequestMethod.GET)
	public @ResponseBody List<EmpVO> getEmpList(@ModelAttribute EmpVO emp){
		log.info("param => {}", emp);
		log.info("getEmpList Start");
		List<EmpVO> list = es.getEmpDeptList2(emp);
		log.info("getEmpList End");
		return list;
	}
	
	@RequestMapping(value="/empdept2", method=RequestMethod.PUT)
	public @ResponseBody Integer doUpdateEmp(@RequestBody EmpVO emp){
		log.info("paramPut => {}", emp);
		return es.updateEmp(emp);
		//return 1;
	}
	
	@RequestMapping(value="/empdept2/{empNo}", method=RequestMethod.DELETE)
	public @ResponseBody Integer doDeleteEmp(@PathVariable("empNo") Integer empNo) {
		log.info("empNo=>{}",empNo);
		return es.deleteEmp(empNo);
	}
	
		
	@RequestMapping(value="/emp", method=RequestMethod.POST)
	public @ResponseBody Integer doInsertEmp(@RequestBody EmpVO emp){
		log.debug("Insert emp => {}", emp);
		return es.insertEmp(emp);
	}
	
	@RequestMapping(value="/emps", method=RequestMethod.POST)
	public @ResponseBody Integer doInsertEmps(@RequestBody List<EmpVO> emps){
		log.debug("Insert emps => {}", emps);
		return es.insertEmps(emps);
	}
	
}
