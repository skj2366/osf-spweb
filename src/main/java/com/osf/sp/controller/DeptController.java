package com.osf.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.osf.sp.service.DeptService;
import com.osf.sp.vo.DeptVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class DeptController {
	
	@Autowired
	private DeptService ds;
	
	@RequestMapping(value="/dept/list", method=RequestMethod.GET)
	public String goPage() {
		log.debug("goPage Start");
		log.info("goPage End");
		return "list";
	}
	
	@RequestMapping(value="/depts", method=RequestMethod.GET)
	public @ResponseBody List<DeptVO> getDeptList(@ModelAttribute DeptVO dept){
		log.info("param => {}", dept);
		log.info("getDeptList Start");
		List<DeptVO> listDept = ds.selectDeptList(dept);
		log.info("getDeptList End");
		return listDept;
	}
	
	@RequestMapping(value="/depts", method=RequestMethod.PUT)
	public @ResponseBody Integer doUpdateDept(@RequestBody DeptVO dept){
		log.info("paramPut => {}", dept);
		return ds.updateDept(dept);
	}
	
	@RequestMapping(value="/depts/{deptNo}", method=RequestMethod.DELETE)
	public @ResponseBody Integer doDeleteDept(@PathVariable("deptNo") Integer deptNo) {
		log.info("deptNo=>{}",deptNo);
		return ds.deleteDept(deptNo);
	}
	
		
	@RequestMapping(value="/dept", method=RequestMethod.POST)
	public @ResponseBody Integer doInsertDept(@RequestBody DeptVO dept){
		log.debug("Insert dept => {}", dept);
		return ds.insertDept(dept);
	}
	
	@RequestMapping(value="/depts", method=RequestMethod.POST)
	public @ResponseBody Integer doInsertDepts(@RequestBody List<DeptVO> depts){
		log.debug("Insert depts => {}", depts);
		return ds.insertDepts(depts);
	}
}
