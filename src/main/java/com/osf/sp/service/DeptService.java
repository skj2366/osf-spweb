package com.osf.sp.service;

import java.util.List;

import com.osf.sp.vo.DeptVO;

public interface DeptService {

	List<DeptVO> selectDeptList(DeptVO dept);
	int updateDept(DeptVO dept);
	int deleteDept(int deptNo);
	public int insertDept(DeptVO dept);
	public int insertDepts(List<DeptVO> depts);
}
