package com.osf.sp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.DeptDAO;
import com.osf.sp.service.DeptService;
import com.osf.sp.vo.DeptVO;

@Service
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DeptDAO ddao; 
	
	@Override
	public List<DeptVO> selectDeptList(DeptVO dept) {
		
		return ddao.selectDeptList(dept);
	}

	@Override
	public int updateDept(DeptVO dept) {

		return ddao.updateDept(dept);
	}

	@Override
	public int deleteDept(int deptNo) {

		return ddao.deleteDept(deptNo);
	}

	@Override
	public int insertDept(DeptVO dept) {
		return ddao.insertDept(dept);
	}

	@Override
	public int insertDepts(List<DeptVO> depts) {
		int cnt = 0;
		for(DeptVO dept:depts) {
			cnt += ddao.insertDept(dept);
		}
		return cnt;
	
	}

	
}
