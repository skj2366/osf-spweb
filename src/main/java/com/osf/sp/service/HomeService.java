package com.osf.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.Emp2DAO;
import com.osf.sp.dao.EmpDAO;
import com.osf.sp.vo.EmpVO;

public class HomeService {

	@Autowired
	private SqlSession ss;
	@Autowired
	private EmpDAO edao;
	@Autowired
	private Emp2DAO e2dao;
	
	public List<String> getStrList(){
		List<String> strList = new ArrayList<String>();
		strList.add("str1");
		strList.add("str2");
		return strList;
	}
	
	public List<EmpVO> getEmpList(){
		return ss.selectList("emp.selectEmpList");
							//앞에있는게 네임스페이스, 뒤에있는게 아이디
							// 즉 , "네임스페이스,아이디"
	}
	
//	public List<Map<String,Object>> getEmpDeptList(){
//		return edao.selectEmpDept();
//	}
	
//	public List<EmpVO> getEmpDeptList2(){
//		return e2dao.selectEmpDept();
//	}
	
//	public void test() {
//		HikariDataSource hds = new HikariDataSource();
//		try {
//			hds.setJdbcUrl("jdbc:oracle:thin:@localhost:1521:xe");
//			hds.setUsername("osfu");
//			hds.setPassword("12345678");
//			hds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//			Connection con = hds.getConnection();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
}
