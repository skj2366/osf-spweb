package com.osf.sp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.Emp2DAO;
import com.osf.sp.vo.EmpVO;

@Service
public class EmpService {

	@Autowired
	private Emp2DAO e2dao;
	
	public List<EmpVO> getEmpDeptList2(EmpVO emp){
		return e2dao.selectEmpDept(emp);
	}
	
	public int updateEmp(EmpVO emp) {
		return e2dao.updateEmp(emp);
	}
	
	public int deleteEmp(Integer empNo) {
		return e2dao.deleteEmp(empNo);
	}
	public int insertEmp(EmpVO emp) {
		return e2dao.insertEmp(emp);
	}
	
	public int insertEmps(List<EmpVO> emps) {
		int cnt = 0;
		for(EmpVO emp:emps) {
			cnt += e2dao.insertEmp(emp);
		}
		return cnt;
	}
}
