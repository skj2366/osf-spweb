package com.osf.sp;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.osf.sp.service.HomeService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class HomeController {
	
	@Autowired
	private HomeService hs;
	
	@RequestMapping(value ="/strList", method =RequestMethod.GET)
	public String getStrList() {
		return "list";
	}
	
//	@RequestMapping(value="/emps", method=RequestMethod.GET)
//	public String getEmpList(Model m) {
//		m.addAttribute("empList", hs.getEmpList());
//		log.info("empList=>{}",hs.getEmpList());
//		return "list";
//	}
	
//	@RequestMapping(value="/empdept", method=RequestMethod.GET)
//	public String getEmpDeptList(Model m) {
//		m.addAttribute("empList2",hs.getEmpDeptList());
//		log.info("empList2=>{}", hs.getEmpDeptList());
//		return "list2";
//	}
	
//	@RequestMapping(value="/empdept2", method=RequestMethod.GET)
//	public String getEmpDeptList2(Model m) {
//		m.addAttribute("empList3",hs.getEmpDeptList2());
//		log.info("empList3=>{}", hs.getEmpDeptList2());
//		return "list3";
//	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		log.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value="/aj",method=RequestMethod.GET)
	public String goPage() {
		return "ajax";
	}
	
}
