package com.osf.sp.vo;

import lombok.Data;

@Data
public class EmpVO {
	private Integer empNo;
	private String eName;
	private String job;
	private Integer deptNo;
//	private Integer mgr;
//	private String hireDate;
//	private Integer sal;
//	private Integer comm;
//	private Integer deptNo;
//	private String id;
//	private String pwd;
//	private String lvl;
//	private String loc;
//	private Integer emp_Cnt;
}
