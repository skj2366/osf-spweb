package com.osf.sp.vo;

import lombok.Data;

@Data
public class DeptVO {

	public Integer deptNo;
	public String dName;
	public String loc;
	//public Integer empCnt;
}
