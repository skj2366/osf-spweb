package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.DeptVO;


public interface DeptDAO {

	List<DeptVO> selectDeptList(DeptVO dept);
	int updateDept(DeptVO dept);
	int deleteDept(int deptNo);
	public int insertDept(DeptVO dept);
}
