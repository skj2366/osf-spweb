package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.EmpVO;

public interface Emp2DAO {

	List<EmpVO> selectEmpDept(EmpVO emp);
	int updateEmp(EmpVO emp);
	int deleteEmp(int empNo);
	public int insertEmp(EmpVO emp);
}
