package com.osf.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.osf.sp.dao.Emp2DAO;
import com.osf.sp.vo.EmpVO;

@Repository
public class Emp2DAOImpl implements Emp2DAO {

	@Autowired
	private SqlSession ss;
	
	@Override
	public List<EmpVO> selectEmpDept(EmpVO emp) {
		return ss.selectList("emp.selectEmpList",emp);
	}

	@Override
	public int updateEmp(EmpVO emp) {
		return ss.update("emp.updateEmp", emp);
	}

	@Override
	public int deleteEmp(int empNo) {
		return ss.delete("emp.deleteEmp",empNo);
	}

	@Override
	public int insertEmp(EmpVO emp) {

		return ss.insert("emp.insertEmp",emp);
	}


}
