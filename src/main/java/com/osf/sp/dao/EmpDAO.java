package com.osf.sp.dao;

import java.util.List;
import java.util.Map;

import com.osf.sp.vo.EmpVO;

public interface EmpDAO {

	List<Map<String, Object>> selectEmpDept(EmpVO emp);
	
}
